import fizzBuzz from "../functions/fizzBuzz.js";

describe("effectuer le kata fizzBuzz du coding dojo ", () => {
  it("test de la function fizzBuzz avec un chiffre qui n'est ni un multiple de 5 ni de 3 et renvoi juste le chiffre", () => {
    expect(fizzBuzz(1)).toEqual(1);
  });
  it("test de la function fizzBuzz avec un multiple de 5 qui renvoi buzz", () => {
    expect(fizzBuzz(5)).toEqual("buzz");
  });
  it("test de la function fizzBuzz avec un multiple de 3 qui renvoi fizz", () => {
    expect(fizzBuzz(3)).toEqual("fizz");
  });
  it("test de la function fizzBuzz avec un multiple de 3 et de 5 qui renvoi fizzBuzz", () => {
    expect(fizzBuzz(15)).toEqual("fizzBuzz");
  });
});
