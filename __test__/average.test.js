import { multiplication, average } from "../functions/multiply.js";

describe("should TDD testing function", () => {
  test("multiplication __test__", () => {
    expect(multiplication(2, 2)).toEqual(4);
  });

  it("should average array function is 0 if empty", () => {
    expect(average([])).toEqual("empty");
  });

  it("should average array function", () => {
    expect(average([1, 2, 3, 4, 5])).toBe(3);
  });
});
