export function multiplication(a, b) {
  return a * b;
}

export function average(item) {
  let average;
  if (item.length <= 0) {
    return "empty";
  } else {
    item.reduce((prev, acc) => {
      average = prev + acc;
      return average;
    });
  }
  return average / item.length;
}
